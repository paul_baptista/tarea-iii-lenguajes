###############################################################################
# Archivo: trees.rb                                                           # 
#                                                                             #                   
# Autores: Francisco Martínez (09-10502),                                     #                               
#          Paul Baptista      (10-10056)                                      #
#                                                                             #
# Implementación de clases para la creación y manejo de árboles binarios y    #
# multicaminos.                                                               #
#                                                                             #
# Universidad Simón Bolívar                                                   #
# Caracas - Venezuela                                                         #
#                                                                             #
# Trimestre Abril-Julio 2015                                                  #
###############################################################################

require './mod_dfs.rb'
require './node_dd.rb'

# Clase BT:
# Define la estructura y métodos principales de BinaryTrees
class BT
  
  include DFS

  attr_accessor :n  # Objeto que guarda el nodo
  attr_reader   :l, # Hijo izquierdo (BT)
                :r  # Hijo derecho (BT)

  def initialize(n, l=nil, r=nil)
    @n = n 
    @l = l
    @r = r
  end

 # Iterador sobre los subárboles inmediados de árbol actual
  def each &block        
    if @l != nil 
      yield @l
    end
    if @r != nil 
      yield @r
    end
  end

end


# Clase BT:
# Define la estructura y métodos principales de RoseTrees
class RT

  include DFS

  attr_accessor :n  # Objeto que guarda el nodo
  attr_reader   :ss # Arreglo de hijos

  def initialize(n, *sons)
    @n = n 
    @ss = sons
  end

  # Iterador sobre los subárboles inmediados de árbol actual
  def each &block
    if (@ss != nil)
      @ss.each {|x| yield x}
    end
  end

end