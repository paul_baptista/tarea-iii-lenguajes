###############################################################################
# Archivo: trees.rb                                                           # 
#                                                                             #                   
# Autores: Francisco Martínez (09-10502),                                     #                               
#          Paul Baptista      (10-10056)                                      #
#                                                                             #
# Módulo "DFS":                                                               #
# Implementación de iterador y mapeo de funciones en recorrido DFS para       #
# estructuras anidadas.                                                       #
#                                                                             #                                                                             #
# Universidad Simón Bolívar                                                   #
# Caracas - Venezuela                                                         #
#                                                                             #
# Trimestre Abril-Julio 2015                                                  #
###############################################################################

module DFS

  # Iterador en recorrido DFS
  def dfs &block
    yield n
    self.each{ |x| x.dfs{ |y| yield y} }
  end

  # Mapeador de bloque a estructura anidada en recorrido DFS
  def dfs! &block
    @n = block.call(n)
    self.each{ |x| x.dfs! &block }
  end

end