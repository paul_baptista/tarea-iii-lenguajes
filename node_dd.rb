###############################################################################
# Archivo: trees.rb                                                           # 
#                                                                             #                   
# Autores: Francisco Martínez (09-10502),                                     #                               
#          Paul Baptista      (10-10056)                                      #
#                                                                             #
# Implementación de clases para la creación y manejo de objetos de tipo Node  #
# a usar en árboles binarios y multicamino.                                   #
#                                                                             #
# Implementación de la clase Visitante y sus subclases: Mirror, Next y Strong;#
# para el operar nodos de BTs y RTs                                           #
#                                                                             #
# Universidad Simón Bolívar                                                   #
# Caracas - Venezuela                                                         #
#                                                                             #
# Trimestre Abril-Julio 2015                                                  #
###############################################################################


# Clase abstracta Visitante, para operar sobre nodos de BTs y RTs
class Visitante; end

# Módulo "Visitado":
#   Implementa, en las clases que se incluya, el método "visitado_por". 
module Visitado

  def visitado_por v
    eval ("v.visitar_#{self.class} self")
  end

end


# Clase que implementa la operación de "Mirror" nodos de BTs y RTs
class Mirror < Visitante

  # Mirror sobre Node
  def visitar_Node node
    Node.new(-node.x,-node.y)
  end

  # Mirror sobre Fixnum
  def visitar_Fixnum num
    -num
  end

 # Mirror sobre Symbol
  def visitar_Symbol sym
    (sym.to_s + sym.to_s.reverse).to_sym
  end

end


# Clase que implementa la operación "Next" nodos de BTs y RTs
class Next < Visitante

  # Next sobre Node
  def visitar_Node node
    Node.new(node.x.next,node.y.next)
  end

  # Next sobre Fixnum
  def visitar_Fixnum num
    num.next
  end

  # Next sobre Symbol
  def visitar_Symbol sym
    var = ""
    sym.to_s.each_char{|c| var = var + c.next}
    return var.to_sym
  end

end


# Clase que implementa la operación "Strong" nodos de BTs y RTs
class Strong < Visitante

  # Strong sobre Node
  def visitar_Node node
    Node.new(100*node.x,100*node.y)
  end

  # Strong sobre Fixnum
  def visitar_Fixnum num
    num*100
  end

  # Strong sobre Symbol
  def visitar_Symbol sym
    sym.upcase
  end

end

# Extensión de la clase "Fixnum" con el módulo "Visitado"
class Fixnum
    include Visitado
end

# Extensión de la clase "Symbol" con el módulo "Visitado"
class Symbol
    include Visitado
end


# Clase Node:
class Node

  include Visitado

  attr_reader :x, :y

  def initialize(x, y)
        @x , @y = x, y
  end
  
  def to_s
    "( #{x} , #{y} )"
  end

end